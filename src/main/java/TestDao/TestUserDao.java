/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestDao;

import dao.UserDao;
import model.User;

/**
 *
 * @author Phai
 */
public class TestUserDao {
    public static void main(String[] args) {
        UserDao user = new UserDao();
        //TestInsertUser
        int id = user.add(new User(-1,"Ei","0861409478","S","123457"));
//        System.out.println("id : "+id);
        //TestLastUser
        User lastUser = user.get(6);
        System.out.println("Last User : "+lastUser);
        //TestSetName
        lastUser.setTel("0822222222");
        //TestUpdateUser
        int row = user.update(lastUser);
        User updateUser = user.get(6);
        System.out.println("Update User :" + updateUser);
        user.delete(row);
        //TestDeleteUser
//        User deleteUser = user.get(row);
//        System.out.println("Delete User : "+ deleteUser);
        


    }
}
