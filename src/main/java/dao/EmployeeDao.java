/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Employee;
import model.UserManage;

/**
 *
 * @author Phai
 */
public class EmployeeDao implements InterfaceDao<Employee> {

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO employee_management (user_management_id,make_sale) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getUserManage_id());
            stmt.setDouble(2, object.getMakeSale());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,\n"
                    + "                            user_management_id,\n"
                    + "                            make_sale\n"
                    + "                       FROM employee_management;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int user_id = result.getInt("user_management_id");
                double make_sale = result.getDouble("make_sale");
                Employee em = new Employee(id, user_id, make_sale);

                list.add(em);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException All" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,user_management_id,make_sale FROM employee_management WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                int user_id = result.getInt("user_management_id");
                double make_sale = result.getDouble("make_sale");
                Employee em = new Employee(pid, user_id, make_sale);
                return em;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        return null;
    }
    
    public Employee getByMaanage(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        Employee em = new Employee(0, 0, 0);
        try {
            String sql = "SELECT id,user_management_id,make_sale FROM employee_management WHERE user_management_id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                int user_id = result.getInt("user_management_id");
                double make_sale = result.getDouble("make_sale");
                em = new Employee(pid, user_id, make_sale);
                return em;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
            return em;
        }
        return em;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM employee_management WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE employee_management SET make_sale  = ? WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, object.getMakeSale());
            stmt.setInt(2, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        UserManageDao umdao = new UserManageDao();
        UserManage um = umdao.get(2);

        ReceiptDao rdao = new ReceiptDao();

        EmployeeDao emdao = new EmployeeDao();
        Employee em = new Employee(6,7, 150);
        
//        emdao.add(em);
//        System.out.println(emdao.get(1));
//        emdao.delete(1);
        

//        emdao.update(em);
        System.out.println(emdao.getAll());

    }
}
