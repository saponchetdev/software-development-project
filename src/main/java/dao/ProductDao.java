/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author Phai
 */
public class ProductDao implements InterfaceDao<Product> {

    @Override
    public int add(Product object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;
        try {
            String insertQuery = "INSERT INTO Product(name,image,price) VALUES(?,?,?)";
            PreparedStatement smt = con.prepareStatement(insertQuery);
            smt.setString(1, object.getName());
            smt.setString(2, object.getImage());
            smt.setDouble(3, object.getPrice());

            int row = smt.executeUpdate();
            ResultSet result = smt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return id;

    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String query = "SELECT id,name,image,price FROM Product";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(query);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String image = result.getString("image");
                double price = result.getDouble("price");
                Product product = new Product(id, name, image, price);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return list;

    }

    @Override
    public Product get(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        try {
            String query = "SELECT id,name,image,price FROM Product WHERE Id=" + id;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(query);
            while (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                String image = result.getString("image");
                double price = result.getDouble("price");
                Product product = new Product(id, name, image, price);
                return product;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return null;

    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        Statement stmt = null;
        conn = db.getConnection();
        int row = 0;
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String deleteProduct = "DELETE from PRODUCT where ID = '"
                    + id + "';";
            row = stmt.executeUpdate(deleteProduct);
            conn.commit();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return row;

    }

    @Override
    public int update(Product object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String updateQuery = "UPDATE Product SET name =?,price =? WHERE Id =?";
            PreparedStatement statement = con.prepareStatement(updateQuery);
            Product product = new Product(object.getId(), object.getName(), object.getImage(), object.getPrice());
            statement.setString(1, product.getName());
            statement.setDouble(2, product.getPrice());
            statement.setInt(3, product.getId());
            row = statement.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return row;
    }

}
