/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Stock;
import model.TableOutOfStock;
import model.TableSale;

/**
 *
 * @author User
 */
public class StockDao implements InterfaceDao<Stock> {

    @Override
    public int add(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO stock_management (name,units,price) VALUES (?,?,?);";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, object.getName());
            statement.setInt(2, object.getUnit());
            statement.setDouble(3, object.getPrice());
            int row = statement.executeUpdate();
            ResultSet result = statement.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Stock> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, units, price FROM stock_management;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                int units = result.getInt("units");
                double price = result.getDouble("price");
                Stock stock = new Stock(id, name, units, price);
                System.out.println(id + " " + name + " " + units + " " + price);
                list.add(stock);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Stock get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, units, price FROM stock_management WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                int units = result.getInt("units");
                double price = result.getDouble("price");
                Stock stock = new Stock(pid, name, units, price);
                return stock;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM stock_management WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE stock_management SET units = ?, price = ? WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getUnit());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());

            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return row;
    }

    public ArrayList<TableOutOfStock> getOutOfStock() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT stock_management.id AS s_id,\n"
                    + "       stock_management.name AS s_name,\n"
                    + "       sum(stock_management.units) AS sumLow\n"
                    + "  FROM stock_management\n"
                    + " GROUP BY stock_management.id\n"
                    + " ORDER BY sumLow ASC;";

            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int stoID = result.getInt("s_id");
                String stoname = result.getString("s_name");
                int unit = result.getInt("sumLow");

                Stock stock = new Stock(stoID, stoname, unit);
                TableOutOfStock ts = new TableOutOfStock(stock, unit);
                list.add(ts);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : Caonnot Select Low");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("ERROR : Connot close database");
        }
        db.close();
        return list;
    }

}
