/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import model.User;
import model.UserManage;

/**
 *
 * @author Phai
 */
public class UserManageDao implements InterfaceDao<UserManage> {

    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    private static final SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public int add(UserManage object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO user_management (user_id,check_in,check_out) VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getUser_id());
            stmt.setString(2, sdf3.format(timestamp));
            stmt.setString(3, sdf3.format(timestamp));
            System.out.println(object.getCheck_in());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return id;

    }

    @Override
    public ArrayList<UserManage> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,user_id,check_in,check_out FROM user_management";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int user_id = result.getInt("user_id");
                Timestamp check_in = result.getTimestamp("check_in");
                Timestamp check_out = result.getTimestamp("check_out");

                UserManage user = new UserManage(id, user_id, check_in, check_out);

                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public UserManage get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,user_id,check_in,check_out FROM user_management WHERE Id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                int user_id = result.getInt("user_id");
                Timestamp check_in = result.getTimestamp("check_in");
                Timestamp check_out = result.getTimestamp("check_out");
                UserManage userM = new UserManage(id, user_id, check_in, check_out);
                return userM;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        return null;
    }
    

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM user_management WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        db.close();
        return row;
    }

    @Override
    public int update(UserManage object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE user_management SET check_in = ?,check_out = ? WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "" + sdf3.format(object.getCheck_in()));
            stmt.setString(2, "" + sdf3.format(object.getCheck_out()));

            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        db.close();
        return row;
    }

}
