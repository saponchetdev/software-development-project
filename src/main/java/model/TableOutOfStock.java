/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Phai
 */
public class TableOutOfStock {
    private String stockID;
    private String stockName;
    private int sum;

    public TableOutOfStock(Stock stock, int sum) {
        this.stockID = ""+stock.getId();
        this.stockName = ""+stock.getName();
        this.sum = sum;
    }

    public String getStockID() {
        return stockID;
    }

    public void setStockID(String stockID) {
        this.stockID = stockID;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "TableOutOfStock{" + "stockID=" + stockID + ", stockName=" + stockName + ", sum=" + sum + '}';
    }
    
    
    
}
