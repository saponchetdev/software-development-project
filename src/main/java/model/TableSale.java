/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Phai
 */
public class TableSale {
    private String productID;
    private String productName;
    private int sum;

    public TableSale(Product product, int sum) {
        this.productID = ""+product.getId();
        this.productName = ""+product.getName();
        this.sum = sum;
    }
    
    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "TableSale{" + "productID=" + productID + ", productName=" + productName + ", sum=" + sum + '}';
    }

    
    
    
    
}
